<?php

namespace LaraDev\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class UserController extends Controller
{
    public function index()
    {
        return "<h1>Listagem de usuáio</h1>";
    }

    public function getData(Request $request)
    {
        var_dump($request);
        return "<h1>Disparada ação de get</h1>";
    }

    public function postData(Request $request)
    {
        var_dump($request);
        return "<h1>Disparada ação de post</h1>";
    }

    public function testPut(Request $request)
    {
        var_dump($request);
        return "<h1>Disparou ação do PUT</h1>";
    }

    public function testPatch(Request $request)
    {
        var_dump($request);
        return "<h1>Disparou ação do Patch</h1>";
    }

    public function testMatch(Request $request)
    {
        echo "<h1>Disparou ação do PUT/PATCH</h1>";
        var_dump($request);
    }

    public function destroy()
    {
        return "<h1>Disparou ação de Delete para o registro 1</h1>";
    }

    public function any()
    {
        return "<h1>Método any que aceita qualquer método de requisição</h1>";
    }

    public function userComments(Request $request, $id, $comment = null)
    {
        //também podemos recuperar as infos da url a partir do Request
        echo "UserController@userComments";
        var_dump($id, $comment, $request);
    }

    public function inspect()
    {
        $route = Route::current();
        $name = Route::currentRouteName();
        $action = Route::currentRouteAction();

        var_dump($route, $name, $action);
    }
}

<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Formulário :: Teste de Rotas</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <div class="container my-5">
        <form action="{{ url('/users/2') }}" method="post" autocomplete="off">
            <!--input necessário por conta da proteção do Laravel no método post -->
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            {{--No html5 só estão disponíveis os métodos http GET e POST e para ultilizar outros podemos--}}
            {{--Usa a seguinte técnina(Form method spoofing)--}}
            {{--<input type="hidden" name="_method" value="PUT"> OU:
            @method('PUT')--}}
            @method('PATCH')
            <div class="form-group">
                <label for="first_name">Primeiro nome:</label>
                <input type="text" id="first_name" name="first_name" class="form-control" value="Matheus">
            </div>

            <div class="form-group">
                <label for="last_name">Segundo nome:</label>
                <input type="text" id="last_name" name="last_name" class="form-control" value="Ribeiro">
            </div>

            <div class="form-group">
                <label for="email">E-mail:</label>
                <input type="email" id="email" name="email" class="form-control" value="matheus@email.com">
            </div>

            <button class="btn btn-primary">Enviar</button>
        </form>
    </div>

    <!-- Bootstrap js -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>

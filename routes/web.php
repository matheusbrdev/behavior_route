<?php

use \Illuminate\Support\Facades\Route;

/*Mudando nomes da url nas rotas*/
Route::resourceVerbs([
    'create' => 'cadastro',
    'edit' => 'editar'
]);

//Route::get('/', function () {
//    return view('welcome');
//});

/**
 *
 * Link da documentação de referência: https://laravel.com/docs/5.7/routing
 *
 * Definição de Rota
 * Route::verbo_http('URI', 'Controller@método');
 *
 * verbo_http = [GET, POST, PUT, PATCH, DELETE, OPTIONS];
 *
 * GET: Utilizado para obter dados do servidor e não altera o estado do recurso.
 *      Quando um formulário GET é disparado, os dados ficam presentes na URL.
 *
 *      Route::get($uri, $callback);
 *
 * POST: Utilizado para criação de recurso ou envio de dados ao servidor para validação.
 *       Os dados ficam no corpo da requisição e não na URL.
 *
 *       Route::post($uri, $callback);
 *
 * Route::put($uri, $callback);
 * Route::patch($uri, $callback);
 * Route::delete($uri, $callback);
 * Route::options($uri, $callback);
 *
 * Passo a passo: Definir rota -> Criar controllador -> Criação de método -> Camada View
 *
 *
 * Route::view('/form', 'form');//view não é um verbo, mas sim um helper
 * GET
 *
 * Route::get('/users/1', 'UserController@index');
 * Route::get('/getData', 'UserController@getData');
 * POST
 *
 * Route::post('/postData', 'UserController@postData');
 *
 * Verbos de atualização:
 * PUT = Usado para atualizar e caso o registro não exista, ele faz a criação
 * Por convenção o PUT ultilizado pra atualização do objeto inteiro
 *
 * PATCH = Usado apenas para atualizar
 * Já o PATCH é ultilizado para atualizar partes do registro
 *
 * PUT
 * Route::put('/users/1', 'UserController@testPut'); ->Não entrará em conflito com a rota get
 * pois são methods diferentes
 *
 * PATCH
 * Route::patch('/users/1', 'UserController@testPatch');
 * Usando mais de um método em uma rota
 *
 * Route::match(['put', 'patch'], '/users/2','UserController@testMatch');
 *
 * DELETE - Tem comportamento parecido com o POST, porém o 'post' é de criação e o 'delete' de deleção
 * Route::delete('/users/1', 'UserController@destroy');
 *
 *
 * Options
 * Lista o cabeçalho para entendermos quais as requisições aceitas naquela URL. Ultilizamos o Postman para esta aula
 *
 * Route::any -> significa qualquer método de requisição
 *
 * Route::any('/users', 'UserController@any');
 * Ver lista de requisições: $ php artisan route:list
 *
 * Trabalhando com Rotas resource(otmizando suas rotas)
 *
 * Route::resource('posts', 'PostController');Ao usar resource, não será necessário informar os métodos
 * $ php artisan make:controller PostController --resource
 *
 * Route::resource('posts', 'PostController')->only(['rota1', 'rota2']) caso queira apenas determinadas rotas
 *
 * Route::resource('posts', 'PostController')->except(['rota1', 'rota2']) implementa todas exeto as passadas no array
 *
 *
 * Route com Closure
 * Route::get('/users', function () {
 * echo "Listagem de users da minha base !";
 * });Assim perdemos o cache nas rotas
 *
 * Route com view
 * Route::view('/form', 'form');
 *
 * Route para tratamento de erros
 * Route::fallback(function () {
 * echo "<h1>Erro 404</h1>";
 * });404 -> chama este método quando não encontra uma determinada rota.
 *O ideal não é passar uma closure, neste caso estamos fazendo testes
 *
 * //Route com redirect
 * Route::redirect('/users/add', url('/form'), 301);1. Origem da requisição;
 * 2. Destino da requisição;
 * 3. Código HTTP de resposta.(301 -> redirecionamento)
 *
 *nomeando rotas
 * Route::get('/posts', 'PostController@index')->name('posts.index');
 * Route::get('/posts/index', 'PostController@indexRedirect')->name('posts.indexRedirect');
 *
 * Tratamento de paramentros - uri dinâmica e resgatando em função
 * Definindo um parâmetro numa rota: {parametro}
 *
 * Route::get('/users/{id}/comments/{comment}', function ($id, $comment){
 * var_dump($id, $comment);
 * }); Usando closure apenas para testes, em caso real use método e controllers
 *
 * //Com parametros opcionais - {parametro?}
 * //Route::get('/users/{id}/comments/{comment?}', function ($id, $comment = null) {
 * //    var_dump($id, $comment);
 * //})->where([
 * //    'id' => '[0-9]+',
 * //    'comment' => '[a-zA-Z]+'
 * //]);//tratamento de parâmetro
 *
 * //Route::get('/users/{id}/comments/{comment?}', 'UserController@userComments')->where([
 * //    'id' => '[0-9]+',
 * //    'comment' => '[a-zA-z]+'
 * //]);
 *
 * Route::get('/users/1', 'UserController@inspect')->name('inspect');
 *
 *
 * //Agrupamento
 *
 * Route::prefix('admin')->group(function () {
 * Route::view('/form', 'form');
 * });
 *
 * Route::name('admin.posts.')->group(function () {
 * Route::get('/admin/posts/index', 'PostController@index')->name('index');
 * Route::get('/admin/posts', 'PostController@show')->name('show');
 * });
 *
 * // Agrupamento com middleware
 * //throtle:10,1 -> nativo do laravel
 * //['throttle:10,1'] -> máximo de 10 requisições em 1 minuto
 *
 * Route::middleware(['throttle:10,1'])->group(function () {
 * Route::view('/form', 'form');
 * });
 *
 *
 * //Agrupamento com namespaces
 *
 * Route::namespace('Admin')->group(function () {
 * Route::get('/users', 'UserController@index');
 * });
 */

//Agrupamento com todos os cenários acima
Route::group([
    'namespace' => 'Admin',
    'prefix' => 'admin',
    'middleware' => ['throttle:10,1'],
    'as' => 'admin.'
],
    function () {
        Route::resource('users', 'UserController');
    }
);
